{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:

let
  inherit (pkgs) lib;
in
pkgs.buildNimSbom {
  inherit src;
  buildInputs = [ pkgs.openssl ];
} ./sbom.json
