{
  pkgs ? import <nixpkgs> { },
  bomPath ? ./sbom.json,
  excludes ? [ ],
  extraConfig ? [],
  extraPaths ? [],
}:

let
  inherit (pkgs)
    lib
    fetchgit
    fetchzip
    runCommand
    xorg
    ;

  fetchers = {
    fetchzip =
      { url, sha256, ... }:
      fetchzip {
        name = "source";
        inherit url sha256;
      };
    git =
      {
        fetchSubmodules ? false,
        leaveDotGit ? false,
        rev,
        sha256,
        url,
        ...
      }:
      fetchgit {
        inherit
          fetchSubmodules
          leaveDotGit
          rev
          sha256
          url
          ;
      };
  };

  filterPropertiesToAttrs =
    prefix: properties:
    lib.pipe properties [
      (builtins.filter ({ name, ... }: (lib.strings.hasPrefix prefix name)))
      (map (
        { name, value }:
        {
          name = lib.strings.removePrefix prefix name;
          inherit value;
        }
      ))
      builtins.listToAttrs
    ];

  buildNimCfg =
    { backend, components, ... }:
    let
      components' = builtins.filter ({ name, ... }: !(builtins.elem name excludes)) components;
      componentSrcDirs = map (
        { properties, ... }:
        let
          fodProps = filterPropertiesToAttrs "nix:fod:" properties;
          fod = fetchers.${fodProps.method} fodProps;
          srcDir = fodProps.srcDir or "";
        in
        if srcDir == "" then fod else "${fod}/${srcDir}"
      ) components';
    in
    runCommand "nim.cfg"
      {
        outputs = [
          "out"
          "src"
        ];
        nativeBuildInputs = [ xorg.lndir ];
      }
      ''
        cat << EOF >> $out
        backend:${backend}
        path:"$src"
        ${lib.strings.concatLines (map (p: ''path:"${toString p}"'') extraPaths)}
        ${lib.strings.concatLines extraConfig}
        EOF
        mkdir -p "$src"
        ${lib.strings.concatMapStrings (d: ''
          lndir "${d}" "$src"
        '') componentSrcDirs}
      '';

  sbom = with builtins; fromJSON (readFile bomPath);
  metaProps = builtins.listToAttrs sbom.metadata.component.properties;
in
buildNimCfg {
  backend = metaProps."nim:backend";
  inherit (sbom) components;
}
