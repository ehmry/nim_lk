# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## Implementation of the purl standard for shortening Github URLs.
## https://github.com/package-url/purl-spec

import
  std/[options, parseutils, pathnorm, strutils, uri]

type

  Type* = enum
    ## Recognized purl types.
    generic,
    github

  Purl* = object
    `type`*: string
    namespace*: string
    name*: string
    version*: string
    qualifiers*: seq[Qualifier]
    subpath*: string

  Qualifier* = tuple[key: string, val: string]

proc addEscaped(result: var string; s: string) =
  for c in s:
    if c in {'a'..'z', 'A'..'Z', '0'..'9', '-', '.', '_', '~', '/', ':'}: result.add c
    else:
      result.add '%'
      result.add c.ord.toHex(2)

proc `$`*(p: Purl): string =
  result = newStringOfCap(32)
  result.add "pkg:"
  if p.`type` == "":
    raise newException(ValueError, "purl does not specify a type")
  result.add p.`type`
  if p.namespace != "":
    result.add '/'
    result.addEscaped p.namespace
  result.add '/'
  if p.`name` == "":
    raise newException(ValueError, "purl does not specify a name")
  result.add p.name
  if p.version != "":
    result.add '@'
    result.addEscaped p.version
  if p.qualifiers != @[]:
    result.add '?'
    for i, (key, val) in p.qualifiers:
      if i > 0: result.add '&'
      result.add key
      result.add '='
      result.addEscaped val
  if p.subpath != "":
    result.add '#'
    result.addEscaped p.subpath

proc parsePurl(result: var Purl; s: string) =
  var s = s
  block: # subpath
    let offSubpath = s.rfind("#")
    if offSubPath >= 0:
      var off = offSubPath.succ
      off.inc s.skipWhile({'/'}, off)
      if offSubPath < s.high:
        result.subpath = s[off..s.high].normalizePath
      s.setLen offSubPath
  block: # qualifiers
    let offQual = s.rfind("?")
    if offQual >= 0:
      var
        key, val: string
        off = offQual.succ
      while off < s.high:
        off.inc s.parseUntil(key, {'=', '&'}, off)
        if off < s.high and s[off] == '=':
          off.inc
          off.inc s.parseUntil(val, '&', off)
        off.inc
        if val != "":
          result.qualifiers.add( (key.toLowerAscii, val.decodeUrl,) )
      s.setLen offQual
  var off = 4 # after "pkg:"
  block: # type
    off.inc s.skipWhile({'/'}, off)
    off.inc s.parseUntil(result.`type`, '/', off)
    result.`type` = result.`type`.toLowerAscii
    off.inc s.skipWhile({'/'}, off)
  block: # version
    let offVersion = s.rfind("@")
    if offVersion >= 0:
      result.version = s[offVersion.succ..s.high].decodeUrl
      s.setLen offVersion
  block: # name
    let offName = s.rfind('/')
    if offName >= 0:
      result.name = s[offName.succ..s.high].decodeUrl
      s.setLen offName
  block: # namespace
    if off < s.high:
      result.namespace = s[off..s.high].decodeUrl.normalizePath

proc parsePurl*(s: string): Option[Purl] =
  if s.startsWith("pkg:"):
    result = some Purl()
    parsePurl(result.get, s)

proc setSourceUrl*(result: var Purl; uri: Uri) =
  ## Update a `Purl` with information from a `Uri`.
  case uri.hostname.toLowerAscii
  of "github.com":
    result.`type` = $github
    var off = 1
    off.inc uri.path.parseUntil(result.namespace, '/', off)
    off.inc
    discard uri.path.parseUntil(result.name, '/', off)
    result.name.removeSuffix ".git"
  else:
    result.`type` = $generic
    case uri.scheme
    of "git", "hg", "svn", "cvs":
      result.qualifiers.add ("vcs_url", $uri,)
    else:
      if uri.scheme.startsWith "git+":
        result.qualifiers.add ("vcs_url", $uri,)
      elif uri.path.rfind("/") < uri.path.rfind(".tar"):
        result.qualifiers.add ("download_url", $uri,)
      else:
        result.qualifiers.add ("repository_url", $uri,)

proc setSourceUrl*(result: var Purl; url: string) =
  ## Update a `Purl` with information from a URL.
  setSourceUrl(result, url.parseUri)

proc unversioned*(purl: Purl): Purl =
  ## Return a copy of `purl` without a version.
  result = purl
  result.version = ""
