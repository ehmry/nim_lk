# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

# https://cyclonedx.org/docs/1.6/json/

import
  std/[algorithm, deques, httpclient, json, options, os, osproc, parseutils, parseopt, streams, strutils, tables, uri],
  pkg/nimblepkg/options,
  pkg/nimblepkg/packageinfo,
  pkg/nimblepkg/packageinfotypes,
  pkg/nimblepkg/packageparser,
  pkg/nimblepkg/version,
  ./nim_lk/purls

const githubPackagesUrl =
  "https://raw.githubusercontent.com/nim-lang/packages/master/packages.json"

var nimbleOptions: Options

let bomBase = %*
  {
    # "$schema": "http://cyclonedx.org/schema/bom-1.6.schema.json",
    "bomFormat": "CycloneDX",
    "specVersion": "1.6",
    "metadata": { },
    "components": [],
    "dependencies": []
  }

proc merge(props: JsonNode, key, val: string) =
  if val != "":
    props.add(%*{ "name": ("nim:"&key), "value": val })

proc merge(props: JsonNode, key: string; values: seq[string]) =
  for val in values:
    props.add(%*{ "name": ("nim:"&key), "value": val })

proc merge(props: JsonNode, key: string; values: Table[string, string]) =
  for (k, v) in values.pairs:
    props.add(%*{ "name": ("nim:"&key&":"&k), "value": v })

proc toMetaComponent(pkgInfo: PackageInfo): JsonNode =
  let typ =
    if pkgInfo.bin.len == 0: "library"
    else: "application"
  let props = newJArray()
  props.merge("skipDirs", pkgInfo.skipDirs)
  props.merge("skipFiles", pkgInfo.skipFiles)
  props.merge("skipExt", pkgInfo.skipExt)
  props.merge("installDirs", pkgInfo.installDirs)
  props.merge("installFiles", pkgInfo.installFiles)
  props.merge("installExt", pkgInfo.installExt)
  props.merge("bin", pkgInfo.bin)
  props.merge("binDir", pkgInfo.binDir)
  props.merge("srcDir", pkgInfo.srcDir)
  props.merge("backend", pkgInfo.backend)
  %*
    {
      "type": typ,
      "bom-ref": ("pkg:nim/" & pkgInfo.name),
      "name": pkgInfo.name,
      "description": pkgInfo.description,
      "version": pkgInfo.basicInfo.version,
      "authors": [ { "name": pkgInfo.author } ],
      "licenses": [ { "license": { "id": pkgInfo.license } } ],
      "properties": props
    }

proc registryCachePath: string =
  result = getEnv("XDG_CACHE_HOME")
  if result == "":
    let home = getEnv("HOME")
    if home == "":
      result = getEnv("TMPDIR", "/tmp")
    else:
      result = home / ".cache"
  result.add "/packages.json"

proc isGitUrl(uri: Uri): bool =
  uri.scheme == "git" or
    uri.scheme.startsWith("git+") or
    uri.path.endsWith(".git") or
    uri.hostname.contains("git")

proc startProcess(cmd: string; cmdArgs: varargs[string]): Process =
  # stderr.writeLine(cmd, " ", join(cmdArgs, " "))
  startProcess(cmd, args = cmdArgs, options = {poUsePath})

type GitPair = object
  `ref`, `rev`: string

proc gitLsRemote(url: string; tagsArg = false): seq[GitPair] =
  var line, rev, refer: string
  var process =
    if tagsArg:
      startProcess("git", "ls-remote", "--tags", url)
    else:
      startProcess("git", "ls-remote", url)
  while process.outputStream.readLine(line):
    var off = 0
    off.inc parseUntil(line, rev, Whitespace, off)
    off.inc skipWhile(line, Whitespace, off)
    refer = line[off..line.high]
    const
      refsTags = "refs/tags/"
      headsTags = "refs/heads/"
    if refer.startsWith(refsTags):
      refer.removePrefix(refsTags)
      result.add GitPair(`ref`: refer, `rev`: rev)
    elif refer.startsWith(headsTags):
      refer.removePrefix(headsTags)
      result.add GitPair(`ref`: refer, `rev`: rev)
  stderr.write(process.errorStream.readAll)
  close(process)
  if tagsArg and result.len == 0:
    result = gitLsRemote(url, not tagsArg)

proc matchRev(url: string; wanted: VersionRange): GitPair =
  if wanted.kind == verSpecial:
    let special = $wanted.spe
    if special[0] == '#':
      result.rev = special[1..special.high]
    else:
      quit("unhandled version " & url & " " & $wanted)
  else:
    let withTags = wanted.kind != verAny
    let pairs = gitLsRemote(url, withTags)
    var resultVersion: Version
    for pair in pairs:
      try:
        var tagVer = pair.`ref`.newVersion
        if tagVer.withinRange(wanted) and resultVersion < tagVer:
          resultVersion = tagVer
          result = pair
      except ParseVersionError: discard
    if result.rev == "" and pairs.len > 0:
      result = pairs[pairs.high]
    doAssert result.rev != "", url

proc collectMetadata(data: JsonNode) =
  let storePath = data{"path"}.getStr
  var packageNames = newSeq[string]()
  for (kind, path) in walkDir(storePath):
    if kind in {pcFile, pcLinkToFile} and path.endsWith(".nimble"):
      var (_, name, _) = splitFile(path)
      packageNames.add name
  if packageNames.len == 0:
    quit("no .nimble files found in " & storePath)
  packageNames.sort()
  data{"packages"} = %packageNames
  var nimbleFilePath = storePath.findNimbleFile(true)
  var pkg = getPkgInfoFromFile(nimbleFilePath, nimbleOptions)
  data{"srcDir"} = %pkg.srcDir

proc prefetchGit(cloneUrl: string; pair: GitPair; subDir = ""): JsonNode =
  assert pair.rev != ""
  var archiveUri = cloneUrl.parseUri
  archiveUri.scheme = "https"
  archiveUri.path.removeSuffix ".git"
  archiveUri.path = archiveUri.path / "archive" / pair.rev & ".tar.gz"
  let client = newHttpClient()
  defer: close(client)
  let
    archiveUrl = $archiveUri
    resp = head(client, archiveUrl)
  if resp.code in {Http200, Http302}:
    # Nix shell can set $TMPDIR to somewhere that doesn't exist.
    getEnv("TMPDIR", "/tmp").createDir()
    var lines = execProcess(
      "nix-prefetch-url",
      args = @[archiveUrl, "--type", "sha256", "--print-path", "--unpack", "--name", "source"],
      options = {poUsePath, poEchoCmd})
    var
      hash, storePath: string
      off: int
    off.inc parseUntil(lines, hash, {'\n'}, off).succ
    if lines.len <= off:
      quit("failed to parse nix-prefetch-url output: " & lines)
    off.inc parseUntil(lines, storePath, {'\n'}, off).succ
    doAssert off == lines.len, "unrecognized nix-prefetch-url output:\n" & lines
    result = %*
      {
        "method":  "fetchzip",
        "path":  storePath,
        "rev":  pair.rev,
        "sha256":  hash,
        "url":  archiveUrl
      }
    if subdir != "":
      result{"subdir"} = %subdir
        # a Nim attribute not used by the fetcher
  else:
    stderr.writeLine "fetch of ", archiveUrl, " returned ", resp.code
    var args = @["--quiet", "--fetch-submodules", "--url", cloneUrl, "--rev", pair.rev]
    stderr.writeLine "prefetch ", cloneUrl
    let dump = execProcess(
      "nix-prefetch-git",
      args = args,
      options = {poUsePath})
    try: result = dump.parseJson
    except CatchableError:
      stderr.writeLine "failed to parse output of nix-prefetch-git ", join(args, " ")
      quit(dump)
    if subdir != "":
      result{"subdir"} = %subdir
        # a Nim attribute not used by the fetcher
    result{"method"} = %"git"
  if pair.`ref` != "":
    result{"ref"} = %pair.`ref`
  collectMetadata(result)

proc prefetchGit(uri: Uri; version: VersionRange): JsonNode =
  var
    uri = uri
    subdir = ""
  uri.scheme.removePrefix("git+")
  if uri.query != "":
    if uri.query.startsWith("subdir="):
      subdir = uri.query[7 .. ^1]
    uri.query = ""
  let cloneUrl = $uri
  cloneUrl.prefetchGit(cloneUrl.matchRev(version), subdir=subdir)

proc selectGitCommit(url: string): GitPair =
  let pairs = gitLsRemote(url)
  stdout.writeLine "select a tag from ", url, ":"
  for pair in pairs:
    stdout.writeLine "\t", pair.`rev`, "\t", pair.`ref`
  let choice = stdin.readLine.strip
  for pair in pairs:
    if choice == pair.`ref` or choice == pair.`rev`:
      return pair

proc containsPackageUri(lockAttrs: JsonNode; pkgUrl: string): bool =
  for e in lockAttrs.items:
    if e{"url"}.getStr == pkgUrl:
      return true

proc containsPackage(lockAttrs: JsonNode; pkgName: string): bool =
  for e in lockAttrs.items:
    for other in e{"packages"}.items:
      if other.getStr == pkgName:
        return true

proc collectRequires(pending: var Deque[PkgTuple]; pkgPath: string) =
  var
    nimbleFilePath = findNimbleFile(pkgPath, true)
    pkg = getPkgInfoFromFile(nimbleFilePath, nimbleOptions)
  for pair in pkg.requires:
    case pair.name
    of "nim", "compiler", "nimrod": discard
    else:
      pending.addLast(pair)

var globalRegistry: JsonNode

proc containsGit(s: string): bool {.inline.} = contains(s, "git")

proc getPackageUrl(name: string): tuple[url: Uri, meth: string] =
  result.url = name.parseUri
  if result.url.scheme != "":
    if result.url.scheme.containsGit or
        result.url.hostname.containsGit or
        result.url.path.endsWith(".git"):
      # TODO: better git heuristic
      result.meth = "git"
  else:
    if globalRegistry.isNil:
      let registryPath = registryCachePath()
      if fileExists(registryPath):
        globalRegistry = parseFile(registryPath)
      else:
        let client = newHttpClient()
        var raw = client.getContent(githubPackagesUrl)
        close(client)
        writeFile(registryPath, raw)
        globalRegistry = parseJson(raw)
    var
      name = name
      i = 0
    while i < globalRegistry.len:
      var e = globalRegistry[i]
      if e["name"].getStr == name:
        if e.hasKey "alias":
          var alias = e["alias"].getStr
          doAssert alias != name
          name = alias
          i = 0
        else:
          try:
            result.url = e["url"].getStr.parseUri
            result.meth = e["method"].getStr
            return
          except CatchableError:
            quit("Failed to parse shit JSON " & $e)
      inc i
    raise newException(KeyError, "package not in global registry: " & name)

proc generateLockfile(directoryPath: string): JsonNode =
  let deps = newJArray()
  var pending: Deque[PkgTuple]
  collectRequires(pending, directoryPath)
  while pending.len > 0:
    let batchLen = pending.len
    for i in 1..batchLen:
      var pkgData: JsonNode
      let pkg = pending.popFirst()
      if pkg.name == "nim" or pkg.name == "compiler" or pkg.name == "nimrod":
        continue
      var uri = parseUri(pkg.name)
      if uri.scheme == "":
        if not deps.containsPackage(pkg.name):
          pending.addLast(pkg)
      elif not deps.containsPackageUri(pkg.name):
        if uri.isGitUrl:
          pkgData = prefetchGit(uri, pkg.ver)
        else:
          quit("unhandled URI " & $uri)
        collectRequires(pending, pkgData{"path"}.getStr)
        deps.add pkgData

    if batchLen == pending.len:
      var
        pkgData: JsonNode
        pkg = pending.popFirst()
        info = getPackageUrl(pkg.name)
      case info.meth
      of "git":
        pkgData = prefetchGit(info.url, pkg.ver)
      else:
        quit("unhandled fetch method " & $info.meth & " for " & $info.url)
      collectRequires(pending, pkgData{"path"}.getStr)
      deps.add pkgData
  return %*{"depends": deps}

proc containsComponent(bom: JsonNode; bomRef: string): bool =
  for c in bom{"components"}.items:
    if c{"bom-ref"}.getStr == bomRef:
      return true

proc containsString(node: JsonNode; s: string): bool =
  for e in node.elems:
    if e.getStr == s:
      return true

type
  DependencyKind = enum dkNimble, dkBom
  Dependency = object
    bomRef: string
    case kind: DependencyKind
    of dkNimble:
      meth: string
      ver: VersionRange
      url: Uri
    of dkBom:
      bom: JsonNode

  Dependencies = Deque[Dependency]

proc toNimPurl(s: string): string =
  let uri = s.parseUri
  var off = uri.path.rfind('/')
  inc off
  result = "pkg:nim/" & uri.path[off..uri.path.high]
  off = result.find('.')
  if off > 0:
    result.setLen(off)

proc queueNimbleDependencies(
    bom: JsonNode; bomRef: string; pkgInfo: PackageInfo; queue: var Dependencies) =
  let dependsOn = newJArray()
  bom{"dependencies"}.add(%*{ "ref": bomRef, "dependsOn": dependsOn })
  for (dn, dv) in pkgInfo.requires:
    case dn
    of "nim", "compiler", "nimrod": discard # TODO
    else:
      var dep = Dependency(kind: dkNimble, bomRef: dn.toNimPurl, ver: dv)
      (dep.url, dep.meth) = getPackageUrl(dn)
        # Nimble package names must be unique
        # within a dependency closure
      if not dependsOn.containsString dep.bomRef:
        dependsOn.add %dep.bomRef
        queue.addLast dep

proc getComponent(bom: JsonNode; bomRef: string; byName = false): JsonNode =
  for e in bom{"components"}.elems:
    if e{"bom-ref"}.getStr == bomRef: return e
  if result.isNil and byName:
    for e in bom{"components"}.elems:
      if e{"name"}.getStr == bomRef: return e
  return newJObject()

proc getDepends(bom: JsonNode; bomRef: string): JsonNode =
  for e in bom{"dependencies"}.elems:
    if e{"ref"}.getStr == bomRef: return e
  return newJObject()

proc queueBomDependencies(bom: JsonNode; bomRef: string; queue: var Dependencies) =
  for depRef in bom.getDepends(bomRef){"dependsOn"}.elems:
    queue.addLast Dependency(kind: dkBom, bomRef: depRef.getStr, bom: bom)
      # process these later

proc addComponent(bom: JsonNode; dep: Dependency, queue: var Dependencies; loadSboms = false) =
  ## Add a component that was discovered as a dependency.
  var depBomRef = dep.bomRef
  case dep.kind
  of dkBom:
    let comp = dep.bom.getComponent(depBomRef)
    bom{"components"}.add comp
    queueBomDependencies(dep.bom, depBomRef, queue)
  of dkNimble:
    var
      extRefs = newJArray()
      props = newJArray()
      fodPath: string
      version = strip($dep.ver, chars={'#'})
    case dep.meth
    of "git":
      let fodData = prefetchGit(dep.url, dep.ver)
      let gitRef = fodData{"ref"}.getStr
      if gitRef != "":
        version = gitRef # store the git ref in the component
      fodPath = fodData{"path"}.getStr
      for (key, val) in fodData.pairs:
        let vs = val.getStr
        if vs != "":
          var prop = %* { "name": ("nix:fod:" & key), "value": vs }
          props.add prop
      case fodData{"method"}.getStr
      of "git":
        extRefs.add(%*{ "url": fodData{"url"}, "type": "vcs" })
      of "fetchzip":
        extRefs.add(%*{ "url": fodData{"url"}, "type": "source-distribution" })
        extRefs.add(%*{ "url": $dep.url, "type": "vcs" })
          # Add the archive URL and the git URL.
      else:
        raiseAssert("unhandled fetch method " & $fodData{"method"})
    else:
      quit("unhandled fetch method " & dep.meth & " for " & $dep.url)
    let sbomPath = fodPath / "sbom.json"
    if loadSboms and sbomPath.fileExists:
      let
        farBom = sbomPath.parseFile
        comp = farBom{"metadata","component"}
        farBomRef = comp{"bom-ref"}.getStr
      if depBomRef == "":
        depBomRef = farBomRef
      elif farBomRef != depBomRef:
        stderr.writeLine "warning: resolved ", depBomRef, " to a bom that uses bom-ref ", farBomRef
      if not bom.containsComponent(depBomRef):
        comp{"externalReferences"} = extRefs
        let farProps = comp{"properties"}
        for key, val in props: farProps{key} = val
          # overlay the FOD properties that we've verified
        assert not bom.containsComponent(depBomRef)
        bom{"components"}.add comp
        queueBomDependencies(farBom, depBomRef, queue)
    else:
      var pkgInfo = fodPath.findNimbleFile(true).getPkgInfoFromFile(nimbleOptions)
      const prefix = "pkg:nim/"
      if depBomRef == "":
        depBomRef = prefix & pkgInfo.name
      elif depBomRef[prefix.len..depBomRef.high] != pkgInfo.name:
        stderr.writeLine("package name ", pkgInfo.name, " does not match ", depBomRef)
      var comp = %*
        {
          "type": "library",
          "bom-ref": depBomRef,
          "name": pkgInfo.name,
          "version": version,
            # git tag as a version, ignore what is in the nimble file
          "externalReferences": extRefs,
          "properties": props
        }
      assert not bom.containsComponent(depBomRef)
      bom{"components"}.add comp
      bom.queueNimbleDependencies(depBomRef, pkgInfo, queue)

proc toVersion(ver: string): Version =
  # Work around upstream.
  if ver.len != 0 and ver[0] notin {'#', '\0'} + Digits:
    newVersion("#" & ver)
  else:
    newVersion(ver)

proc toDependency(arg: string): Dependency =
  if arg.parsePurl.isSome:
    quit("""specifying dependencies by purl is not supported""")
  let info = arg.getPackageUrl()
  if info.meth != "git":
    quit("unhandled distribution method " & $info.meth & " for " & arg)
  let
    urlStr = $info.url
    gitPair = urlStr.selectGitCommit()
  Dependency(
      kind: dkNimble,
      meth: info.meth,
      ver: VersionRange(kind: verEq, ver: gitPair.ref.toVersion),
      url: info.url,
  )

proc addComponents(sbom: JsonNode; args: openarray[string]) =
  var queue: Dependencies
  for arg in args:
    let dep = arg.toDependency
    sbom.addComponent(dep, queue, true)
  while queue.len > 0:
    let dep = queue.popFirst()
    if not sbom.containsComponent(dep.bomRef):
      sbom.addComponent(dep, queue, true)

proc generateBom(nimblePath: string): JsonNode =
  ## Dependencies are processed using a queue.
  ## Deps are appended to the queue in the order that
  ## they appear within a package and each level of the
  ## tree is processed before moving towards an edge.
  ## This has the effect that if a package is specified multiple
  ## times in the tree with the same name and different versions
  ## then the package specified closest to the root is selected.
  var queue: Dependencies
  var pkgInfo =
    if nimblePath.fileExists:
      nimblePath.getPkgInfoFromFile(nimbleOptions)
    else:
      findNimbleFile(nimblePath, true).getPkgInfoFromFile(nimbleOptions)
  result = bomBase
  result{"metadata","component"} = pkgInfo.toMetaComponent
  result.queueNimbleDependencies(result{"metadata","component","bom-ref"}.getStr, pkgInfo, queue)
  while queue.len > 0:
    let dep = queue.popFirst()
    if not result.containsComponent(dep.bomRef):
      result.addComponent(dep, queue)

proc replaceProperty(props: JsonNode; key, val: string): bool =
  for pair in props.elems:
    if pair{"name"}.getStr == key:
      if pair{"value"}.getStr == val:
        return false
      else:
        pair{"value"} = %val
        return true
  props.add(%*{ "name": key, "value": val })
  true

proc updateFromSources(bom: JsonNode; bomRef, sourcesPath: string) =
  # TODO
  discard

proc updateFodData(bom: JsonNode; bomRef, version: string; fodData: JsonNode): bool =
  let
    comp = bom.getComponent(bomRef)
    props = comp{"properties"}
  if comp{"version"}.getStr != version:
    comp{"version"} = %version
    result = true
  for extRef in comp{"externalReferences"}.elems:
    if extRef{"type"}.getStr == "source-distribution":
      if extRef{"url"} != fodData{"url"}:
        extRef{"url"} = fodData{"url"}
        result = true
  for (key, val) in fodData.pairs:
    let vs = val.getStr
    if vs != "":
      result = props.replaceProperty("nix:fod:" & key, vs) or result

proc update(bom: JsonNode; bomRef: string; byName: bool): bool =
  let comp = bom.getComponent(bomRef, byName)
  assert not comp.isNil
  var bomRef = comp{"bom-ref"}.getStr
  assert not bomRef.isNil
  var suitableRefFound = false
  let extRefs = comp{"externalReferences"}
  for extRef in extRefs.elems:
    if result: break
    if extRef{"type"}.getStr == "vcs":
      suitableRefFound = true
      stderr.writeLine "updating ", extRef
      let url = extRef{"url"}.getStr
      if not url.parseUri.isGitUrl:
        stderr.writeLine("assuming ", url, " is a git URL")
      let gitPair = url.selectGitCommit()
      let pkgData = url.prefetchGit gitPair
      result = bom.updateFodData(bomRef, gitPair.`ref`.strip(chars={'v'}), pkgData)
      if result:
        bom.updateFromSources(bomRef, pkgData{"path"}.getStr)
  if not suitableRefFound:
    stderr.writeLine "cannot update from \"externalReferences\": ", extRefs

type Mode = enum
  unspecifiedMode,
  lockFileMode,
  bomFromNimbleMode,
  listComponentsMode,
  updateComponentMode,
  addComponentMode,

const nimbleModes = {
    lockFileMode,
    bomFromNimbleMode,
    updateComponentMode,
    addComponentMode,
  }

proc findSbom(path: string): string =
  result = path
  if result.dirExists:
    result = result / "sbom.json"
  if not result.fileExists:
    quit("no file to parse at " & result)

proc parseSbom(path: string): JsonNode = path.findSbom.parseFile

proc main =
  var
    componentArgs: seq[string]
    packagePath: string
    mode = unspecifiedMode
    recursive = true

  proc setMode(m: Mode) =
    if mode notin {unspecifiedMode, mode}:
      quit("nim_lk mode can be specified only once")
    mode = m

  for kind, key, val in getopt():
    case kind
    of cmdArgument:
      case key
      of "bom-from-nimble", "nimble-to-bom", "nimble-to-sbom", "sbom-from-nimble":
        setMode bomFromNimbleMode
      of "nix-from-nimble", "nimble-to-nix":
        mode = lockFileMode
      of "list-components", "components", "comps":
        mode = listComponentsMode
      of "update-component", "update":
        mode = updateComponentMode
      of "add-component", "add":
        mode = addComponentMode
      else:
        if packagePath != "":
          quit("no more than a single package path may be specified")
        packagePath = key
    of cmdShortOption, cmdLongOption:
      case key
      of "component", "comp", "c":
        if val == "":
          quit("a component must be specified with " & key & ":")
        componentArgs.add val
      of "recursive", "r":
        recursive = true
      of "flat":
        recursive = false
      else:
        quit("unrecognized flag " & key)
    of cmdEnd: discard

  if mode == unspecifiedMode:
    mode = lockFileMode
      # TODO: require an explicit mode

  if packagePath == "":
    packagePath = getCurrentDir()

  if mode in nimbleModes:
    nimbleOptions = initOptions()
    nimbleOptions.nimBin = findExe("nim")
    if packagePath.fileExists:
      packagePath = packagePath.parentDir
        # packagePath must be a directory

  case mode
  of lockFileMode:
    stdout.writeLine generateLockfile(packagePath).pretty
  of bomFromNimbleMode:
    stdout.writeLine generateBom(packagePath).pretty
  of listComponentsMode:
    let bom = packagePath.parseSbom()
    for comp in bom{"components"}.elems:
      stdout.writeLine comp{"bom-ref"}.getStr
  of updateComponentMode:
    var updated = false
    if componentArgs.len == 0:
      quit("update requires at least one --component:… argument")
    let
      sbomPath = packagePath.findSbom()
      bom = sbomPath.parseFile()
    for s in componentArgs:
      assert not bom.isNil
      if bom.update(s, byName=true):
        updated = true
      else:
        stderr.writeLine "no update to ", s
    if updated:
      var text = bom.pretty
      text.add '\n'
      sbomPath.writeFile text
    else:
      stderr.writeLine "no updates to ", sbomPath
  of addComponentMode:
    if componentArgs.len == 0:
      quit("add requires at least one --component:… argument")
    let
      sbomPath = packagePath.findSbom()
      bom = sbomPath.parseFile()
    bom.addComponents(componentArgs)
    var text = bom.pretty
    text.add '\n'
    sbomPath.writeFile text

  of unspecifiedMode:
    raiseAssert "mode unspecified"

main()
