# Completions for the `nim_lk` command

set --local commands nimble-to-nix nimble-to-sbom update

complete --command nim_lk \
	--no-files

complete --command nim_lk \
	--condition "not __fish_seen_subcommand_from $commands" \
	--arguments "$commands"

complete --command nim_lk \
	--condition "__fish_seen_subcommand_from nimble-to-nix nimble-to-sbom" \
	--arguments '(__fish_complete_directories (commandline -ct) "Nimble package directory")'

complete --command nim_lk \
	--condition "__fish_seen_subcommand_from update" \
	--arguments '(__fish_complete_suffix sbom.json)' \
	--short-option c --long-option component
