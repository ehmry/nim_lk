import
  std/[json, options, os, unittest],
  ../src/nim_lk/purl

proc findTestVectors: string =
  var parent = getCurrentDir()
  while result != "/":
    result = parent / "tests/purl-spec-test-suite-data.json"
    if fileExists result: return
    parent = parent.parentDir
  raiseAssert "Could not find test vectors"

proc loadTestVectors: JsonNode =
  findTestVectors().parseFile()

suite "purl-spec":
  let testVectors = loadTestVectors()
  for v in testVectors:
    test v["purl"].getStr:
      checkpoint v["description"].getStr
      if not v["is_invalid"].getBool:
        let p = v["purl"].getStr.parsePurl.get
        check p.subpath == v["subpath"].getStr
        block:
          let qualifiers = v["qualifiers"]
          if qualifiers.kind == JNull:
            check p.qualifiers.len == 0
          else:
            let q = newJObject()
            for (key, val) in p.qualifiers:
              q[key] = %val
            check q == qualifiers
        check p.`type` == v["type"].getStr
        check p.version == v["version"].getStr
        check p.name == v["name"].getStr
        check p.namespace == v["namespace"].getStr
        let
          test = $p
          ctrl = v["canonical_purl"].getStr
        check test == ctrl

suite "setSourcesUrl":
  const vectors = [
    ("preserves",
     "http://git.syndicate-lang.org/ehmry/preserves-nim.git",
     "pkg:generic/preserves?repository_url=http://git.syndicate-lang.org/ehmry/preserves-nim.git"
     ),
    ("coap"
    , "https://codeberg.org/eris/nim-coap.git"
    , "pkg:generic/coap?repository_url=https://codeberg.org/eris/nim-coap.git"
    ),
    ("nimbom"
    , "https://git.sr.ht/~ehmry/nimbom"
    , "pkg:generic/nimbom?repository_url=https://git.sr.ht/~ehmry/nimbom"
    ),
    ("cps"
    , "https://github.com/nim-works/cps"
    , "pkg:github/nim-works/cps"),
    ("stew"
    , "https://github.com/status-im/nim-stew#3c91b8694e15137a81ec7db37c6c58194ec94a6a"
    , "pkg:github/status-im/nim-stew"),
    ("npeg"
    , "https://github.com/zevv/npeg.git"
    , "pkg:github/zevv/npeg"),
  ]
  for (name, url, ctrl) in vectors:
    test url:
      var p = Purl(name: name)
      p.setSourceUrl url
      var purl = $p
      check purl == ctrl
