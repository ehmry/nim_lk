let
  pkgs = import <nixpkgs> { };
  pkg = import ./default.nix { inherit pkgs; };
in
pkg.overrideAttrs (
  {
    nativeBuildInputs ? [ ],
    ...
  }:
  {
    nativeBuildInputs = nativeBuildInputs ++ [ pkgs.yajsv ];
  }
)
